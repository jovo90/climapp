# REACT
## Instalar
- Node.js
- Npm
- Yarn
- Vscode 
- VScode-icons

## Aplicación Clima
```
npx create-react-app climapp
```

Entrar a carpeta: 
```
cd climapp
```

Iniciar app: 

```
yarn start
```

Iconos Clima
```
npm install --save react-weathericons
```
```
yarn add react-weathericons
```

Si falla el yarn reinstalar npm
```
rm -rf node_modules/ && npm i
```
Prop-types: para validación de datos
```
yarn add prop-types
```
https://github.com/facebook/prop-types

Css tool
https://www.cssmatic.com/

Material UI
https://material-ui.com

```
npm install @material-ui/core
```

Lib Convertir unidades
```
yarn add convert-units
```
Plugin VSCODE
- eslint: Valida sintaxis tags

React Flexbox:
http://roylee0704.github.io/react-flexbox-grid/
```
npm install --save react-flexbox-grid
```
```
yarn add react-flexbox-grid
```
https://momentjs.com/docs/
```
npm install moment
```

Liberia para el tiempo


## Instalar REDUX
```
npm install --save redux
```
```
npm install --save react-redux
```

