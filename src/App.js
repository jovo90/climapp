import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'
import './App.css';
import ForecastExtendedContainer from './containers/ForecastExtendedContainer'
import {Grid,Row, Col} from 'react-flexbox-grid'
import { MuiThemeProvider } from '@material-ui/core/';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import LocationListContainer from './containers/LocationListContainer'

let theme = createMuiTheme();
theme = responsiveFontSizes(theme);

const cities =[
    "buenos aires",
    "chicago",
    "madrid",
    "new york",
    "bogota",
]
class App extends Component
{
    render() {

        return (
            <MuiThemeProvider theme={theme}>
                <Grid>
                    <Row>
                        <AppBar title="Weather App" position='sticky'>
                            <Toolbar>
                                <Typography variant='subtitle1' color='inherit'>
                                    Weather App
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <LocationListContainer cities={cities} >
                            </LocationListContainer>
                        </Col>

                        <Col xs={12} md={6}>
                            <Paper zdepth={4}>
                                <div className="detail">
                                    <ForecastExtendedContainer></ForecastExtendedContainer>
                                </div>
                            </Paper>
                        </Col>
                    </Row>
                </Grid>
                </MuiThemeProvider>
        );
    }
}

export default App;
