import convert from "convert-units";

import {
    SUN, 
    CLOUD,
    RAIN,
    SNOW,
    THUNDER, 
    DRIZZLE
} from "./../constants/climas";

const getTemp = kelvin => {
    return Number(convert(kelvin).from("K").to("C").toFixed(2));
}

const getEstadoClima = weather => {
    const {id} = weather;

    if( id < 300 )
    {
        return THUNDER
    }
    else if( id < 400 )
    {
        return DRIZZLE
    }
    else if( id < 600 )
    {
        return RAIN
    }
    else if( id < 700 )
    {
        return SNOW
    }
    else if( id === 800 )
    {
        return SUN
    }
    else 
    {
        return CLOUD
    }
}

const transformWheather = clima_data => {
    const { humidity, temp } = clima_data.main;
    const { speed } = clima_data.wind;
    const estado = getEstadoClima(clima_data.weather[0]);
    const temp1 = getTemp(temp);

    const data = {
        humedad: humidity,
        temp: temp1,
        estado,
        viento: `${speed} m/s`
    }

    return data;
}

export default transformWheather;