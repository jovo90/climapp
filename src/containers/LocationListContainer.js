import React, { Component } from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { setCity } from './../actions'
import LocationList from "./../components/LocationList";

class LocationListContainer extends Component {

    handleSelectedLocation = city => {
        this.props.setCity(city)
    }

    render() {
        return (
            <div>
                <LocationList 
                        cities={this.props.cities} 
                        onSelectedLocation={this.handleSelectedLocation} >
                        </LocationList>
            </div>
        );
    }
} 

LocationListContainer.propTypes = {
    setCity: PropTypes.func.isRequired,
};

const mapDispatchProps = dispatch => ({
    setCity: value => dispatch(setCity(value))
});

export default connect(null, mapDispatchProps)(LocationListContainer);
