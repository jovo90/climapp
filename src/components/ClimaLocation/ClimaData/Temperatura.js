import React from "react";
import WeatherIcons from "react-weathericons";
import PropTypes from 'prop-types';

import {
    CLOUD,
    SUN,
    RAIN,
    SNOW,
    THUNDER,
    DRIZZLE
} from "./../../../constants/climas";

const icons = {
    [CLOUD]: "cloud",
    [SUN]: "day-sunny",
    [RAIN]: "rain",
    [SNOW]: "snow",
    [THUNDER]: "day-thunderstorm",
    [DRIZZLE]: "day-showers",
}
const getIconClima = estadoClima => {
    const icon = icons[estadoClima];
    const sizeIcon = "4x";

    if(icon)
        return <WeatherIcons className="wicon" name={icon} size={sizeIcon} />;
    else 
     return <WeatherIcons className="wicon" name={"day-sunny"} size={sizeIcon} />;
}

const Temperatura = ({temp, estado}) => (
    <div className="divTemp">
        {getIconClima(estado)} 
        <span className="temp" >{temp} </span> 
        <span className="grados" >Cº</span> 
    </div>
);

Temperatura.propTypes = {
  temp: PropTypes.number.isRequired, 
  estado: PropTypes.string.isRequired  
};

export default Temperatura; 