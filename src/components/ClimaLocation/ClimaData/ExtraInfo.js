import React from "react";
import PropTypes from 'prop-types';

const ExtraInfo = ({humedad,viento}) => 
(
    <div className="divExtraInfo">
        <span className="humedad">Viento {`${humedad}%`}</span>
        <span className="viento">Humedad {viento}</span>
    </div>
)

ExtraInfo.propTypes = {
    humedad: PropTypes.number.isRequired,
    viento: PropTypes.string.isRequired,
}

export default ExtraInfo;  