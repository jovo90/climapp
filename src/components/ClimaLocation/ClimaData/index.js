import React from "react";
import PropTypes from 'prop-types';
import ExtraInfo from "./ExtraInfo";
import Temperatura from "./Temperatura";
import './styles.css';

const ClimaData = ({data:{temp,estado,humedad,viento}}) => {

    return (
    <div className="divClimaData">
        <Temperatura temp={temp} estado={estado}/>
        <ExtraInfo humedad={humedad} viento={viento}/>
    </div>
    );
}

ClimaData.propTypes = {
    data: PropTypes.shape({
        temp: PropTypes.number.isRequired,
        estado: PropTypes.string.isRequired,
        humedad: PropTypes.number.isRequired,
        viento: PropTypes.string.isRequired,
    })
}

export default ClimaData; 