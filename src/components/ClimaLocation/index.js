import React, {Component} from "react";
import CircularProgress from '@material-ui/core/CircularProgress'
import {PropTypes} from 'prop-types'
import transformWeather from './../../services/transformWeather'
import getUrlWeatherByCity from './../../services/getUrlWeatherByCity'
import Location from "./Location";
import ClimaData from "./ClimaData";

class ClimaLocations extends Component{

    constructor(props){
        super(props);

        const {city} = props;

        this.state = {
            city,
            data: null
        };
        console.log("constructor")
    }

    componentDidMount() {
        this.updateClick();
    }

    componentDidUpdate(prevProps, prevState) {
        //console.log("componentDidUpdate")
    }

    updateClick = () => {
        const api_clima = getUrlWeatherByCity(this.state.city)

        fetch(api_clima).then( resolve => {
            return resolve.json();
        }).then( data => {

            //console.log("Resultado Click")

            const newClima = transformWeather(data);

            this.setState({
                data: newClima
            });
        });
    }

    render(){
        const {onWeatherLocationClick} = this.props
        const {city, data} = this.state
        return (
            <div onClick={onWeatherLocationClick}>
                <Location city={city}></Location>
                { data ? <ClimaData data={data}></ClimaData> : <CircularProgress size={50}/>}
            </div>
        );
    }
    
}
//se exporta la variable para que sea usada por fuera -> App.js

ClimaLocations.propTypes = {
    city: PropTypes.string.isRequired,
    onWeatherLocationClick: PropTypes.func,
}

export default ClimaLocations; 