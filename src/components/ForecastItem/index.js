import React from 'react'
import PropTypes from 'prop-types'
import WeatherData from './../ClimaLocation/ClimaData'

const ForecasItem = ({weekDay, hour, data }) => (
    <div>
        <h2>{weekDay} - {hour} hs</h2>
         <WeatherData data={data}></WeatherData>
    </div>
)

ForecasItem.propTypes = {
    weekDay: PropTypes.string.isRequired
}

export default ForecasItem