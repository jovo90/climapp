import React, {Component} from 'react'
import PropTypes from 'prop-types'
import ForecastItem from './ForecastItem'
import transformForecast from './../services/transformForecast'
import './styles.css'

/*
const days = [
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
    'Domingo'
]

const data = {
    temp: 10,
    estado: 'normal',
    humedad: 10,
    viento: 'normal',
}*/

export const api_key = "62e7c29152c84312876a7850b3184844";
export const url = "https://api.openweathermap.org/data/2.5/forecast";

class ForecastExtended extends Component{

    constructor(){
        super()
        this.state = { forecastData: null }
    }

    componentDidMount(){
        this.updateCity(this.props.city)
    }

    //al cambiar los props
    componentWillReceiveProps(nextProps) {
        if(nextProps.city !== this.props.city ){
            this.setState({forecastData:null})
            this.updateCity(nextProps.city)
        }
    }
    
    updateCity = city => {
        const url_forecast = `${url}?q=${city}&appid=${api_key}`
        //fetch or axios https://github.com/axios/axios
        fetch( url_forecast ).then(
            data => (data.json())
        ).then(
            weather_data => {
                const forecastData = transformForecast(weather_data)
                this.setState({forecastData})
            }
        )
    }

    renderForecastItemDays(forecastData){
        return forecastData.map( forecast => (
            <ForecastItem 
                key={`${forecast.weekday}${forecast.hour}`}
                weekDay={forecast.weekday} 
                hour={forecast.hour} 
                data={forecast.data}>
                </ForecastItem>) )
    }

    renderProgress = () => {
        return "<h3>Cargando Pronostico extendido...</h3>"
    }
    
    render(){
        const {city} = this.props
        const {forecastData} = this.state
        return (
        <div>
            <h2 className='forecast-title' >Pronostico Extendido para {city.toUpperCase()}</h2>
            {forecastData ?
                this.renderForecastItemDays(forecastData):
                this.renderProgress()}
        </div>)
    }
}

ForecastExtended.propTypes = {
    city: PropTypes.string.isRequired
}

export default ForecastExtended